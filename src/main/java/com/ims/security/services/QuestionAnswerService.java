package com.ims.security.services;

import java.util.List;

import com.ims.models.Question;

public interface QuestionAnswerService {

	List<Question> getQuestionsAnswer();

}
