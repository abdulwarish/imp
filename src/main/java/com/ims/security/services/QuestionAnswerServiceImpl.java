package com.ims.security.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ims.models.Question;
import com.ims.repository.QuestionAnswerRepository;

@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService {

	@Autowired
	private QuestionAnswerRepository questRepo;
	
	@Override
	public List<Question> getQuestionsAnswer() {
		return questRepo.findAllQuestionAndAnswer();
	}

}
