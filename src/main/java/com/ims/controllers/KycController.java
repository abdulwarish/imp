package com.ims.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class KycController {
	private static String UPLOADED_FOLDER = "D://uploadedfiles//";

	@RequestMapping(value = "/uploadMultipleFiles ", method = RequestMethod.POST)
	public String uploadMultipleFiles(@RequestParam("file") MultipartFile[] files) {


		String status = "";
		File dir = new File(UPLOADED_FOLDER);
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			try {
				byte[] bytes = file.getBytes();

				if (!dir.exists())
					dir.mkdirs();

				File uploadFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
				BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(uploadFile));
				outputStream.write(bytes);
				outputStream.close();

				status = status + "You successfully uploaded file=" + file.getOriginalFilename();
			} catch (Exception e) {
				status = status + "Failed to upload " + file.getOriginalFilename() + " " + e.getMessage();
			}
		}
		return status;
	}

}
