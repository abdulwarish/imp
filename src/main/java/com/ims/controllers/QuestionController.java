package com.ims.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ims.models.Question;
import com.ims.security.services.QuestionAnswerService;

@RestController
public class QuestionController {
	
	@Autowired
	private QuestionAnswerService questionservice;
	
	@GetMapping("/getQuesitonAnswers")
	public List<Question> getQuestionAnswer(Model model) {
		//Question quest = new Question();
		return questionservice.getQuestionsAnswer();
	}
	
}
