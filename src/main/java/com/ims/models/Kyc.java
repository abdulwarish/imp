package com.ims.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "kyc")
public class Kyc {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
     
    @Column(name = "upload_photo")
    private String uploadPhoto;
     
    @Column(name = "upload_proof")
    private String uploadProof;
     
    @Column(name = "selfie_img")
    private String selfieImg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUploadPhoto() {
		return uploadPhoto;
	}

	public void setUploadPhoto(String uploadPhoto) {
		this.uploadPhoto = uploadPhoto;
	}

	public String getUploadProof() {
		return uploadProof;
	}

	public void setUploadProof(String uploadProof) {
		this.uploadProof = uploadProof;
	}

	public String getSelfieImg() {
		return selfieImg;
	}

	public void setSelfieImg(String selfieImg) {
		this.selfieImg = selfieImg;
	}
}
