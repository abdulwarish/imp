package com.ims.models;

import java.security.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * (`id`,
`text`,
`position`,
`score`,
`created`,
`updated`)

 * */

@Entity
@Table(name="answer")
public class Answer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	@Column
	private String text;
	
	@Column
	private String position;
	
	@Column(name="questions_section_id")
	private long questionsSectionId;
	
	@Column(name = "question_no")
	private String questioNo;
	
	@Column
	private String score;
	
	@Column
	private Timestamp created;
	
	@Column
	private Timestamp updated;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public long getQuestionsSectionId() {
		return questionsSectionId;
	}

	public void setQuestionsSectionId(long questionsSectionId) {
		this.questionsSectionId = questionsSectionId;
	}
}