package com.ims.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ims.models.Question;

@Repository
public interface QuestionAnswerRepository extends JpaRepository<Question, Long> {

	@Query(value = "SELECT DISTINCT q.text AS Question,q.question_no,a.questions_section_id, a.text AS Answer,a.question_no AS AnswerNumber,q.id\r\n"
			+ "FROM Question q\r\n"
			+ "LEFT OUTER JOIN ANSWER A\r\n"
			+ "ON q.question_no  = :a.question_no\r\n"
			+ "AND q.id = :a.question_no")
	List<Question> findAllQuestionAndAnswer();

}
