package com.ims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestmentManagementsystem {

	public static void main(String[] args) {
		SpringApplication.run(InvestmentManagementsystem.class, args);
	}

}
