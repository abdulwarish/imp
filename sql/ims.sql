/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.1.44-community : Database - sencap
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sencap` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sencap`;

/*Table structure for table `answer` */

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` tinyblob,
  `position` varchar(255) DEFAULT NULL,
  `questions_section_id` bigint(20) DEFAULT NULL,
  `question_no` varchar(20) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `updated` tinyblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `answer` */

insert  into `answer`(`id`,`created`,`position`,`questions_section_id`,`question_no`,`score`,`text`,`updated`) values 
(1,NULL,'',1,'1',NULL,'Under $50,000',NULL),
(2,NULL,'',1,'1',NULL,'$50,000 to $100,000',NULL),
(3,NULL,'',1,'1',NULL,'$100,001 to $150,000',NULL),
(4,NULL,'',1,'1',NULL,'$150,001 to $250,000',NULL),
(5,NULL,'',1,'1',NULL,'Over $250,000',NULL),
(6,NULL,'',1,'2',NULL,'To decrease.',NULL),
(7,NULL,NULL,1,'2',NULL,'To remain the same.',NULL),
(8,NULL,NULL,1,'2',NULL,'To increase by less than 10%.',NULL),
(9,NULL,NULL,1,'2',NULL,'To increase by more than 10%.',NULL),
(10,NULL,NULL,1,'2',NULL,'Unsure.',NULL),
(11,NULL,NULL,1,'3',NULL,'Under $100,000',NULL),
(12,NULL,NULL,1,'3',NULL,'$100,001 to $250,000',NULL),
(13,NULL,NULL,1,'3',NULL,'$250,001 to $500,000',NULL),
(14,NULL,NULL,1,'3',NULL,'$500,000 to $1,000,000',NULL),
(15,NULL,NULL,1,'3',NULL,'Over $1,000,000',NULL),
(16,NULL,NULL,1,'4',NULL,'Less than 10%',NULL),
(17,NULL,NULL,1,'4',NULL,'10% to 25%',NULL),
(18,NULL,NULL,1,'4',NULL,'25% to 50%',NULL),
(19,NULL,NULL,1,'4',NULL,'Over 50%',NULL),
(20,NULL,NULL,1,'5',NULL,'No',NULL),
(21,NULL,NULL,1,'5',NULL,'Yes, at least the equivalent to 3 months of income',NULL),
(22,NULL,NULL,1,'5',NULL,'Yes, at least the equivalent to 6 months of income',NULL),
(23,NULL,NULL,1,'5',NULL,'Yes, at least the equivalent to 12 months of income',NULL),
(24,NULL,NULL,1,'5',NULL,'Yes, more than 12 months of income',NULL),
(25,NULL,NULL,1,'6',NULL,'Less than $25,000',NULL),
(26,NULL,NULL,1,'6',NULL,'$25,000 to $50,000',NULL),
(27,NULL,NULL,1,'6',NULL,'$50,000 to $100,000',NULL),
(28,NULL,NULL,1,'6',NULL,'$100,000 to $250,000',NULL),
(29,NULL,NULL,1,'6',NULL,'Over $250,000',NULL),
(30,NULL,NULL,0,'',NULL,NULL,NULL);

/*Table structure for table `kyc` */

DROP TABLE IF EXISTS `kyc`;

CREATE TABLE `kyc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `selfie_img` varchar(255) DEFAULT NULL,
  `upload_photo` varchar(255) DEFAULT NULL,
  `upload_proof` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kyc` */

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` tinyblob,
  `label` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `questions_section_id` bigint(20) DEFAULT NULL,
  `question_no` varchar(20) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `updated` tinyblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `question` */

insert  into `question`(`id`,`created`,`label`,`position`,`questions_section_id`,`question_no`,`text`,`updated`) values 
(1,NULL,NULL,'',1,'1','Which of the following best represents your current annual household income (including pensions)?',NULL),
(2,NULL,NULL,'',1,'2','What is your expectation of future household income over the next 5 years?',NULL),
(3,NULL,NULL,'',1,'3','After deducting any loan or mortgage balances and including all your assets (primary residence included) which one of\r\nthe following best represents your household’s overall net worth?',NULL),
(4,NULL,NULL,'',1,'4','How much of your overall household net worth (question 3) is held in liquid assets? (Liquid Assets being those that are\r\ncash or easily changed into cash, e.g., listed securities, short term bonds, mutual funds or money market funds.)',NULL),
(5,NULL,NULL,'',1,'5','Do you have emergency cash reserves to provide for unexpected expenses?',NULL),
(6,NULL,NULL,'',1,'6','What are your expected contributions to your investment portfolio over the next 12 months?',NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`) values 
(1,'ROLE_USER'),
(2,'ROLE_MODERATOR'),
(3,'ROLE_ADMIN');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
  CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

insert  into `user_roles`(`user_id`,`role_id`) values 
(1,1),
(1,2);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `sure_name` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`country_code`,`email`,`first_name`,`password`,`phone_number`,`sure_name`,`username`) values 
(1,'INDIA','mohammad@gmail.com','Abdul','$2a$10$xNCy.U4Ttc26qv8w.cjB7uActyUSuXlm2G8vXs9Rb/1T00W9uHEIG','9161619963','Hello','Abdul Waris');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
